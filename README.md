DUO JWT Fetchers
================

These tools can fetch duo JWTs for you.

Usage of the shell version looks like this:

    jwt-fetcher.sh |xclip

you'll be prompted for your pid, password, and 2fa method (which can be
unspecified).

The python version uses a shamelessy neutered copy of getpass.py, here called
```sane_input.py```, which echos the user's input. Theoretically, it does work
for Windows, too, but I havent't tested whether it echos, since I don't use it.
This doesn't yet ask for a 2fa method, but if you want one, you can certainly
append it to the password input.

Both python and bash/python versions depend on pyJwt and requests; everything
else are standard python libraries. I may eventually add a requirements.txt.

By the way, this does not support legacy versions of python.
