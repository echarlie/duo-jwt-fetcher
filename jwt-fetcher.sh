#!/bin/bash

# note: this doesn't have /any/ error handling in the python part

read -e -p'PID: ' PID
read -e -s -p'PID Password: ' PASS
printf '\n'
read -e -s -p'[push,phone,$OTP]: ' TOKEN
printf '\n'

if [[ -z $PID ]] || [[ -z $PASS ]]; then
	echo "You didn't enter PID or Password" >&2
	exit 1
fi
if [[ -n $TOKEN ]]; then
	PASS=$PASS,$TOKEN
else
	echo "You'll now get a 2-factor push or phone call" >&2
fi


/usr/bin/env python3 <<EOF
import requests
from jwt import decode
print(decode(requests.post('https://apps.middleware.vt.edu/ws/v1/tokens', auth=('$PID','$PASS')).content, verify=False)['password'])
EOF



