#!/usr/bin/env python3

import os
import sys
from jwt import decode
from getpass import getpass
import requests

endpoint='https://apps.middleware.vt.edu/ws/v1/tokens'
uupid=input('PID: ')
password=getpass('PID Password: ')

#print('recieved PID: {}'.format(uupid), file = sys.stderr)
#print('recieved password: {}'.format(password), file = sys.stderr)

print("You'll now get a 2-factor push of phone call", file = sys.stderr )

r = requests.post(endpoint, auth=(uupid, password ))
d = decode(r.content, verify=False)
print(d['password'])


